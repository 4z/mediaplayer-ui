#!/usr/bin/python 

import sys
import os
import asyncio
import json
import hashlib
import threading
import dbus
import argparse
from queue import Queue
from threading import Lock
from dbus.mainloop.glib import DBusGMainLoop
import gi.repository.GLib
from aiohttp import web
from mpris2 import get_players_uri, Player, TrackList
from urllib.parse import urlparse, unquote
from concurrent.futures import FIRST_COMPLETED
import aiohttp

def get_default_theme_html_root():
    return os.path.join(sys.prefix, 'share', 'mediaplayer-ui', 'html-ui')
html_root = get_default_theme_html_root()
def get_theme_index_file(): 
    return os.path.join(html_root, 'mediaplayer.html')

artimg_to_url = {}
url_to_artimg = {}

player_ifc=''
player_ifc_lock = Lock()

def set_player_ifc(ifc):
    global player_ifc
    global player_ifc_lock
    player_ifc_lock.acquire()
    player_ifc = ifc;
    player_ifc_lock.release()

def get_player_ifc():
    global player_ifc
    global player_ifc_lock
    player_ifc_lock.acquire()
    ifc = player_ifc;
    player_ifc_lock.release()
    return ifc

def find_player():
    return next(get_players_uri('.+vlc'))

def get_player():
    return Player(dbus_interface_info={'dbus_uri': get_player_ifc()})

def get_tracklist():
    return TrackList(dbus_interface_info={'dbus_uri': get_player_ifc()})

async_loop=None
ws_ques_lock = Lock()
ws_event_queues = []

def add_event_queue(queue):
    ws_ques_lock.acquire()
    ws_event_queues.append(queue)
    ws_ques_lock.release()
    print('client queue added')
    
def remove_event_queue(queue):
    ws_ques_lock.acquire()
    ws_event_queues.remove(queue)
    ws_ques_lock.release()
    print('client queue removed')

def enqueue_event(evt):
    print('enqueueing event')
    ws_ques_lock.acquire()
    for q in ws_event_queues:
        asyncio.run_coroutine_threadsafe(q.put(evt), async_loop)
    ws_ques_lock.release()

def prop_handler(self, *args, **kw):
    #print(args, kw)
    event = {}
    send = False
    if 'PlaybackStatus' in args[0]:
        event['name'] = 'PlaybackStatus'
        event['data'] = args[0]['PlaybackStatus']
        send = True
    elif 'Metadata' in args[0]:
        event['name'] = 'CurrentTrack'
        event['data'] = get_track_info(args[0]['Metadata'])
        send = True
    elif 'LoopStatus' in args[0]:
        event['name'] = 'LoopStatus'
        event['data'] = args[0]['LoopStatus']
        send = True
    elif 'Shuffle' in args[0]:
        event['name'] = 'Shuffle'
        event['data'] = args[0]['Shuffle']
        send = True
    elif 'Tracks' in args[1]:
        #tracklist updated
        event['name'] = 'TracksChanged'
        send = True
    if send:
        enqueue_event(json.dumps(event))

async def handle_player_events(event_queue, ws):
    evt = await event_queue.get()
    await ws.send_str(evt)
    #print("sending event: ", evt)

async def handle_ws_events(ws):
    msg = await ws.receive()
    if msg.type == aiohttp.WSMsgType.CLOSE:
        return False
    elif msg.type == aiohttp.WSMsgType.ERROR:
        print('ws connection closed with exception %s' % ws.exception())
        return False
    
    if ws.closed:
        return False
    else:
        return True

async def websocket_handler(request):

    ws = web.WebSocketResponse()
    await ws.prepare(request)

    event_queue = asyncio.Queue()
    add_event_queue(event_queue)
    
    t1 = asyncio.create_task(handle_player_events(event_queue, ws))
    t2 = asyncio.create_task(handle_ws_events(ws))
    tasks = [t1, t2]
    while True:
        done, tasks = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)
        
        if t1 in done:
            # player handler finished
            t1 = asyncio.create_task(handle_player_events(event_queue, ws))
            tasks.add(t1)
        else:
            # ws handler finished
            if t2.result() == False:
                print('Connection closed')
                break
            t2 = asyncio.create_task(handle_ws_msgs(ws))
            tasks.add(t2)
    
    t1.cancel()
    t2.cancel()
    remove_event_queue(event_queue)

    return ws

async def toggle_play_handler(request):
    res = {}
    if get_player_ifc():
        player = get_player();
    else:
        res['res'] = 'Error'
        res['reason'] = 'PlayerDisconnected'
        return web.Response(text=json.dumps(res))
    
    status = player.PlaybackStatus
    if status == 'Playing':
        res['res'] = 'Ok'
        res['PlaybackStatus'] = 'Paused'
        player.Pause()
    elif status == 'Paused' or status == 'Stopped':
        res['res'] = 'Ok'
        res['PlaybackStatus'] = 'Playing'
        player.Play()
    else:
        res['res'] = 'Error'
    
    return web.Response(text=json.dumps(res))

def get_track_artimg(metadata):
    if 'mpris:artUrl' in metadata and urlparse(metadata['mpris:artUrl']).scheme == 'file':
        img_path = unquote(urlparse(metadata['mpris:artUrl']).path)
        if img_path in artimg_to_url: 
            return '/artimg/' + str(artimg_to_url[img_path])
        elif os.path.exists(img_path):
            img_id = hashlib.sha1(img_path.encode('utf-8')).hexdigest()

            artimg_to_url[img_path] = img_id
            url_to_artimg[img_id] = img_path
            return '/artimg/' + img_id
    return ''
       

def get_track_info(metadata):
    if not 'mpris:trackid' in metadata:
        return {}
    info = {
        "trackid": metadata['mpris:trackid'],
        "length": int(metadata['mpris:length']),
    }
    
    if 'xesam:title' in metadata:
        info['title'] = metadata['xesam:title']
    else:
        info['title'] = '/no title/'
    
    if 'xesam:artist' in metadata:
        info['artist'] = metadata['xesam:artist']
    else:
        info['artist'] = '/no artist/'
    
    if 'xesam:album' in metadata:
        info['album'] = metadata['xesam:album']
    else:
        info['album'] = '/no album/'
    
    art_img = get_track_artimg(metadata)
    if art_img != '': 
        info['cover'] = art_img
    
    return info

def get_current_track_id():
    player = get_player();
    metadata = player.Metadata
    if 'mpris:trackid' in metadata:
        return metadata['mpris:trackid']
    else:
        return ''

def get_current_track_info():
    player = get_player();
    metadata = player.Metadata
    return get_track_info(metadata)

async def next_handler(request):
    res = {}
    if get_player_ifc():
        player = get_player();
    else:
        res['res'] = 'Error'
        res['reason'] = 'PlayerDisconnected'
        return web.Response(text=json.dumps(res))
    
    player.Next()
    res['res'] = 'Ok'
    res['CurrentTrack'] = get_current_track_info()
    return web.Response(text=json.dumps(res))
    
async def prev_handler(request):
    res = {}
    if get_player_ifc():
        player = get_player();
    else:
        res['res'] = 'Error'
        res['reason'] = 'PlayerDisconnected'
        return web.Response(text=json.dumps(res))
    
    player.Previous()
    res['res'] = 'Ok'
    res['CurrentTrack'] = get_current_track_info()
    return web.Response(text=json.dumps(res))

async def artimg_handler(request):
    img_id = request.match_info['tail']
    if img_id in url_to_artimg:
        return web.FileResponse(url_to_artimg[img_id])
    raise web.HTTPNotFound

async def index_handler(request):
    return web.FileResponse(get_theme_index_file())

async def set_var_handler(request):
    res = {}    
    if get_player_ifc():
        player = get_player();
    else:
        res['res'] = 'Error'
        res['reason'] = 'PlayerDisconnected'
        return web.Response(text=json.dumps(res))

    if 'name' in request.rel_url.query and 'val' in request.rel_url.query:
        name = request.rel_url.query['name']
        val = request.rel_url.query['val']
        
        if name == 'PlaybackStatus':
            if val == 'Playing':
                player.Play()
                res['res'] = 'Ok'
            elif val == 'Paused':
                player.Pause()
                res['res'] = 'Ok'
            elif val == 'Stopped':
                player.Stop()
                res['res'] = 'Ok'
        elif name == 'LoopStatus':
            if val == 'None' or val == 'Track' or val == 'Playlist':
                player.LoopStatus = val
                res['res'] = 'Ok'
        elif name == 'Shuffle':
            if val == '0':
                player.Shuffle = False
                res['res'] = 'Ok'
            elif val == '1':
                player.Shuffle = True
                res['res'] = 'Ok'
        elif name == 'Position':
            try:
                position = int(val)
                if position >= 0:
                    player.SetPosition(get_current_track_id(), position)
                    event = {}
                    event['name'] = 'Seeked'
                    event['data'] = position
                    enqueue_event(json.dumps(event))
                    res['res'] = 'Ok'
            except:
                pass
        elif name == 'Volume':
            try:
                volume = float(val)
                if volume >= 0 and volume <= 1:
                    player.Volume = volume
                    res['res'] = 'Ok'
            except:
                pass
        if 'data' in res:
            res['res'] = 'Ok'
    
    if not 'res' in res:
        res['res'] = 'Error'
    
    return web.Response(text=json.dumps(res))
    
async def get_var_handler(request):
    res = {}
    if get_player_ifc():
        player = get_player();
    else:
        res['res'] = 'Error'
        res['reason'] = 'PlayerDisconnected'
        return web.Response(text=json.dumps(res))
    
    if 'name' in request.rel_url.query:
        name = request.rel_url.query['name']
        
        if name == 'PlaybackStatus':
            res['data'] = player.PlaybackStatus
        elif name == 'CurrentTrack':
            res['data'] = get_current_track_info()
        elif name == 'LoopStatus':
            res['data'] = player.LoopStatus
        elif name == 'Shuffle':
            res['data'] = player.Shuffle
        elif name == 'Position':
            length = 0
            t = get_current_track_info();
            if 'length' in t:
                length = t['length']
            res['data'] = {'Position': player.Position, 'Length': length}
        elif name == 'Volume':
            res['data'] = player.Volume
        elif name == 'Playlist':
            tl = get_tracklist();
            playlist = []
            current_track_id=get_current_track_id()
            
            for t in tl.GetTracksMetadata(tl.Tracks):
                track_info = get_track_info(t)
                if t['mpris:trackid'] == current_track_id:
                    track_info['current'] = True
                playlist.append(track_info)
                
            res['data'] = playlist
        
        if 'data' in res:
            res['res'] = 'Ok'
    else:
        res['res'] = 'Error'
    
    return web.Response(text=json.dumps(res))

def name_watcher(name, new_owner, old_owner):
    if name and not new_owner and old_owner:
        if not get_player_ifc():
            uri = ''
            try:
                uri = find_player()
                set_player_ifc(uri)
            except:
                print('Player not found')
                pass
            
            if uri:
                # generate event
                event = {}
                event['name'] = 'PlayerConnected'
                enqueue_event(json.dumps(event))
                connect_prop_handler(uri)
    elif name and new_owner and not old_owner:
        my_ifc = get_player_ifc()
        if my_ifc and my_ifc == name:
            print('current players interface dissappeared')
            set_player_ifc('')
            
            # generate event
            event = {}
            event['name'] = 'PlayerDisconnected'
            enqueue_event(json.dumps(event))

def connect_prop_handler(uri):
    bus = dbus.SessionBus()
    obj = bus.get_object(uri, '/org/mpris/MediaPlayer2')
    iface = dbus.Interface(obj, 'org.freedesktop.DBus.Properties')
    iface.connect_to_signal('PropertiesChanged', prop_handler)

def glib_loop(loop):
    bus = dbus.SessionBus(loop)
    bus.add_signal_receiver(name_watcher, signal_name='NameOwnerChanged',
                        dbus_interface='org.freedesktop.DBus')
    
    ifc = get_player_ifc()
    if (ifc):
        #player = Player(dbus_interface_info={'dbus_uri': ifc})
        #player.PropertiesChanged = prop_handler
        connect_prop_handler(ifc)
    
    gi.repository.GLib.MainLoop().run()

def main(argv=None):
    
    ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument("-d", "--html-root", required=False, help="webui html root directory", default=get_default_theme_html_root())
    ap.add_argument("-a", "--bind-address", required=False, help="webui bind address", default="127.0.0.1")
    ap.add_argument("-p", "--port", required=False, help="webui port", type = int, default=8080)
    args = vars(ap.parse_args())
    
    global html_root
    html_root = args['html_root']
    
    app = web.Application()
    app.router.add_get('/', index_handler)
    app.router.add_route('GET', '/artimg/{tail:.*}', artimg_handler)
    app.router.add_get('/ws_events', websocket_handler)
    app.router.add_get('/toggle_play', toggle_play_handler)
    app.router.add_get('/next', next_handler)
    app.router.add_get('/prev', prev_handler)
    app.router.add_get('/set_var', set_var_handler)
    app.router.add_get('/get_var', get_var_handler)
    
    app.router.add_static(prefix='/',
                        path=args['html_root'])
    loop = DBusGMainLoop(set_as_default=True)
    try:
        ifc = find_player()
        print('Player is: ', ifc)
        set_player_ifc(ifc)
    except:
        print('Player not found')
        pass
    
    global async_loop
    async_loop = asyncio.get_event_loop()
    threading.Thread(target=glib_loop, args=(loop,)).start()
    
    web.run_app(app, host=args['bind_address'], port=args['port'])
    
    return 0

if __name__ == '__main__':
    main()
