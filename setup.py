import os
from setuptools import setup

dest_dir_prefix= os.path.join('share', 'mediaplayer-ui')
datadir = os.path.join('html-ui')
datafiles = [(os.path.join(dest_dir_prefix, d), [os.path.join(d, f) for f in files])
    for d, folders, files in os.walk(datadir)]

setup(
   name='mediaplayer_ui',
   version='0.1',
   description='mpris mediaplayer web interface',
   author='Matej Mok',
   author_email='matej.mok@gmail.com',
   packages=['mpris_webapi'],
   package_dir={'mpris_webapi': 'mpris-webapi'},
   data_files=datafiles,
   include_package_data=True,
   install_requires=['mpris2', 'aiohttp'],
   entry_points = {
        "console_scripts": [        # command-line executables to expose
            "mediaplayer_ui = mpris_webapi.mpris_webapi:main",
        ],
    }
)
