/*
 * Icons by:
 * Font Awesome – http://fontawesome.io/
 * Those Icons – https://www.flaticon.com/authors/those-icons
 * EpicCoders – https://www.flaticon.com/authors/epiccoders
 * Smashicons – https://www.flaticon.com/authors/smashicons
 */

var trackPosition = 0, trackLength = 0;
var positionRelease = -1;
var playbackTimer = null;
var tracks = new Array();
var trackIndices = [];
var currentTrackIndex = 0;
// controls 
var audioPlayer = document.querySelector('#content');
var playpauseBtn = audioPlayer.querySelector('#play-btn');
var progress = audioPlayer.querySelector('.progress');
var sliders = audioPlayer.querySelectorAll('.slider');
var currentTime = audioPlayer.querySelector('#current-time');
var totalTime = audioPlayer.querySelector('#total-time');

var draggableClasses = ['pin'];
var currentlyDragged = null;

function playback_tick() {
    trackPosition+=100000;
    update_progress(trackPosition, trackLength);
    
    if (trackPosition >= trackLength) {
        console.log('Doing clearInterval');
        clearInterval(playbackTimer);
        playbackTimer = null;
    }
}

function set_track(index, animate) {
    trackid = tracks[index];
    currentTrackIndex = index;
    cover = $("li[trackid='" + trackid + "'] img").attr('src');
    console.info("Scrolling to " + index + ' [' + trackid + ']');
    $("#background").css('background-image', 'url('+cover+')');
    $('.jcarousel').jcarousel('scroll', $("li[trackid='" + trackid + "']"), animate);
}

function process_player_event(evt) {
    if (! 'name' in evt) {
        console.log("Invalid event");
        return ;
    }
    
    switch(evt['name']) {
        case 'PlaybackStatus':
            update_play_btn(evt['data']);
            break;
        case 'CurrentTrack':
            trackid = evt.data.trackid;
            console.log("Track changed to " + trackid);
            for (i = 0; i < tracks.length; i++) {
                if (trackid == tracks[i] && currentTrackIndex != i) {
                    set_track(i, true);
                }
            }
            // after track change VLC does not report if PlaybackStatus changed
            player_get_var('PlaybackStatus', function(res) {
                update_play_btn(res);
            } );
            break;
        case 'TracksChanged': 
            update_tracks();
        case 'LoopStatus':
            update_repeat_btn(evt['data']);
            break;
        case 'Shuffle':
            update_shuffle_btn(evt['data']);
            break;
        case 'Seeked':
            trackPosition = evt['data'];
            update_progress(trackPosition, trackLength);
            positionRelease = -1;
            break;
        case 'PlayerConnected':
            console.log('PlayerConnected')
            hide_info();
            player_init();
            break
        case 'PlayerDisconnected':
            show_info('player-disconnected');
            break
        default:
            console.log("Unknown event name: '" + evt['name'] + "'");
    }
}

function show_info(what) {
    console.log('show info');
    $("#background").css('background-image', "url('assets/audio-icon.png')");
    $("#"+what).show().siblings("li").hide();
    if ($('#info').is(':hidden')) {
        $('#info').show();
    }
    if ($('.jcarousel').is(':visible')) {
        $('.jcarousel').hide();
    }
}

function hide_info() {
    console.log('hide info')
    $('#info').hide();
    $('.jcarousel').show();
}

function update_play_btn(state) {
    if (state == 'Playing') {
        player_get_var('Position', function(res) {
            trackPosition = res['Position'];
            trackLength = res['Length'];
            totalTime.textContent = format_time(trackLength);
            update_progress(res['Position'], res['Length']);
            if (playbackTimer != null) {
                clearInterval(playbackTimer);
            }
            playbackTimer = setInterval(playback_tick, 100)
        });
        $('#play-btn i').addClass('fa-pause');
        $('#play-btn i').removeClass('fa-play');
        
    } else if (state == 'Paused' || state == 'Stopped' ) {
        $('#play-btn i').addClass('fa-play');
        $('#play-btn i').removeClass('fa-pause');
        
        if (playbackTimer != null) {
            clearInterval(playbackTimer);
            playbackTimer = null;
        }
        player_get_var('Position', function(res) {
            trackPosition = res['Position'];
            trackLength = res['Length'];
            totalTime.textContent = format_time(trackLength);
            update_progress(res['Position'], res['Length']);
        });
    } else {
        console.log("Unknown playback status: '" + state + "'")
    }
}

function update_shuffle_btn(shuffle) {
    if (shuffle) {
        if (!$("#random-btn").hasClass('active'))
            $("#random-btn").addClass('active');
    } else {
        if ($("#random-btn").hasClass('active'))
            $("#random-btn").removeClass('active');
    }  
}

function update_repeat_btn(state) {
    if (state == 'None') {
        if ($("#repeat-btn").hasClass('active'))
            $("#repeat-btn").removeClass('active');
    } else {
        if (!$("#repeat-btn").hasClass('active'))
            $("#repeat-btn").addClass('active');
    }
}

function format_time_sec(time) {
  var min = Math.floor(time / 60);
  var sec = Math.floor(time % 60);
  return min + ':' + ((sec<10) ? ('0' + sec) : sec);
}

function format_time(time) {
  var min = Math.floor(time / 1000 / 1000 / 60);
  var sec = Math.floor(time / 1000 / 1000 % 60);
  return min + ':' + ((sec<10) ? ('0' + sec) : sec);
}

function update_progress(position, length) {
    percent = (position / length) * 100;
    progress.style.width = percent + '%';
  
    currentTime.textContent = format_time(position);
}

function player_set_var(name, val) {
    $.get( "/set_var", { 'name': name, 'val': val }, function(data) { 
        res = JSON.parse(data);
        if (res['res'] != 'Ok') {
            console.log("set_var failed");
            if (res['res'] == 'Error' && 'reason' in res && res['reason'] == 'PlayerDisconnected') {
                show_info('player-disconnected');
            }
        }
    } );
}

function player_get_var(name, callback) {
    $.get( "/get_var", { 'name': name }, function(data) {
        res = JSON.parse(data);
        if (res['res'] != 'Ok') {
            console.log("get_var failed");
            if (res['res'] == 'Error' && 'reason' in res && res['reason'] == 'PlayerDisconnected') {
                show_info('player-disconnected');
            }
        } else {
            callback(res['data']);
        }
    });
}

function player_method(name, callback=null) {
    $.get('/' + name, function(data) {
//         console.log('data: ' + data);
        res = JSON.parse(data);
        if (res['res'] != 'Ok') {
            console.log("get_var failed");
            if (res['res'] == 'Error' && 'reason' in res && res['reason'] == 'PlayerDisconnected') {
                show_info('player-disconnected');
            }
        } else {
            if (callback)
                callback(res['data']);
        }
    });
}

function remove_track(trackid, reload=true) {
    $("li[trackid='" + trackid + "']").remove();
    if (reload)
        $('.jcarousel').jcarousel('reload');
}

function make_song_item(song) {
    art_img = 'assets/audio-icon.png';
    if ("cover" in song) {
        art_img = song.cover;
    }
    
    return '<li class="song" trackid="' + song.trackid + '">' + 
           '<img src="' + art_img + '">' +
           '<p class="song-title">' + song.title + '</p>' +
           '<p class="song-artist">' + song.artist + '</p>' + 
           '</li>';
    
}

function update_tracks() {
    player_get_var('Playlist', function(data){
        if (data.length == 0) {
            show_info('playlist-empty');
        } else {
            hide_info();
        }
        
        if (tracks.length == 0) {
            tracks = [];
            songList = $("#songs");
            //songList = audioPlayer.querySelector('#songs');
            $.each(data,function(key, song) {
                tracks.push(song.trackid);
                trackIndices[song.trackid] = key;
                songList.append(make_song_item(song));
                
                if ('current' in song && song.current == true) {
                    if (currentTrackIndex != key) {
                        currentTrackIndex = key;
                        set_track(currentTrackIndex, false);
                    }
                    trackLength = song.length;
                    totalTime.textContent = format_time(trackLength)
                }
            });
            $('.jcarousel').jcarousel('reload');
        } else {
            deleted = [];
            added = [];
            after = [];
            ids = {};
            for (i = 0; i < data.length; i++) {
                ids[data[i].trackid] = true;
            }
            
            for (i = 0; i < tracks.length; i++) {
                if (ids[tracks[i]] == undefined) {
                    console.log('Deleted ' + tracks[i]);
                    deleted.push(trackIndices[tracks[i]]);
                }
            }
            start_i = currentTrackIndex;
            for (i = 0; i < deleted.length; i++) {
                console.log('Deleting ' + tracks[deleted[i]-i])
                remove_track(tracks[deleted[i]-i], false);
                
                if (currentTrackIndex > deleted[i]-i) {
                    currentTrackIndex--;
                }
                // update indices
                for (j = deleted[i]-i+1; j<tracks.length; j++) {
                    trackIndices[tracks[j]]--;
                }
                delete trackIndices[tracks[deleted[i]-i]];
                tracks.splice(deleted[i]-i, 1);
            }
            if (currentTrackIndex != start_i)
                set_track(currentTrackIndex, false);
            console.log('CurrentIndex ' + currentTrackIndex);
            
            for (i = 0; i < data.length; i++) {
                if (trackIndices[data[i].trackid] == undefined) {
                    //console.log('Added ' + data[i].trackid);
                    added.push(data[i]);
                    if (i == 0)
                        after.push(0);
                    else
                        after.push(data[i-1].trackid);
                }
            }
            
            for (i = 0; i < added.length; i++) {
//                 console.log("Add " + added[i].trackid + " after " + after[i]);
//                 console.log('Adding after ' + after[i]);
                $("li[trackid='" + after[i] + "']").after(make_song_item(added[i]));
                if (typeof(after[i]) != 'string')
                    tracks.splice(0, 0, added[i].trackid);
                else {
//                     console.log('splice at ' + trackIndices[after[i]]+1);
                    tracks.splice(trackIndices[after[i]]+1, 0, added[i].trackid);
                }
                trackIndices[added[i].trackid] = trackIndices[after[i]]+1;
            }
            console.log(tracks);
            console.log(trackIndices);
            $('.jcarousel').jcarousel('reload');
            
        }
    });
}

function player_init() {
    
    console.log('player init');
    
    for (i = 0; i < tracks.length; i++) {
        remove_track(tracks[i], false);
    }
    
    trackPosition = 0, trackLength = 0;
    if (playbackTimer)
        clearInterval(playbackTimer);
    playbackTimer = null;
    tracks = new Array();
    trackIndices = [];
    currentTrackIndex = -1;
    
    update_tracks();
    
    player_get_var('PlaybackStatus', function(res) {
        update_play_btn(res);
    });
    
    player_get_var('LoopStatus', function(res) {
        update_repeat_btn(res);
    });
        
    player_get_var('Shuffle', function(res) {
        update_shuffle_btn(res);
    });
}

$(document).ready(function () {
    player_init();
    
    socket = new WebSocket("ws://" + location.host + "/ws_events");
    socket.onmessage = function (e) {
        evt = JSON.parse(e.data);
        process_player_event(evt)
    };
    
    $('.jcarousel').jcarousel({
            center: true,
	});
    //$('.jcarousel').jcarouselSwipe({method: 'scrollIntoView'});
    
    $('.jcarousel').on('jcarousel:reloadend', function(event, carousel) {
        // set position to current track
        //console.log(document.getElementById("songs").offsetWidth);
        //set_track(currentTrackIndex, false);
        //#setTimeout(function() { set_track(currentTrackIndex, false); }, 500);
    });
});

/*
 * Replace all SVG images with inline SVG
 */
jQuery('img[src$=".svg"]').each(function(){
	var $img = jQuery(this);
	var imgID = $img.attr('id');
	var imgClass = $img.attr('class');
	var imgURL = $img.attr('src');

	jQuery.get(imgURL, function(data) {
		// Get the SVG tag, ignore the rest
		var $svg = jQuery(data).find('svg');

		// Add replaced image's ID to the new SVG
		if(typeof imgID !== 'undefined') {
			$svg = $svg.attr('id', imgID);
		}
		// Add replaced image's classes to the new SVG
		if(typeof imgClass !== 'undefined') {
			$svg = $svg.attr('class', imgClass+' replaced-svg');
		}

		// Remove any invalid XML tags as per http://validator.w3.org
		$svg = $svg.removeAttr('xmlns:a');

		// Replace image with new SVG
		$img.replaceWith($svg);

	}, 'xml');

});

// Previous slide
$('#previous-btn').click(function() {
    player_method('prev');
});

// Next slide
$('#next-btn').click(function() {
    player_method('next');
});

// Play Icon Switcher
$('#play-btn').click(function() {
    player_method('toggle_play');
});

// Repeat button
$('#repeat-btn').click(function() {
    if ($('#repeat-btn').hasClass('active')) {
        player_set_var('LoopStatus', 'None');
    } else {
        player_set_var('LoopStatus', 'Playlist');
    }
});

// Shuffle button
$('#random-btn').click(function() {
    if ($('#random-btn').hasClass('active')) {
        player_set_var('Shuffle', '0');
    } else {
        player_set_var('Shuffle', '1');
    }
});

// Menu
$("#menu-btn").click(function() {
	$("#content-wrap").addClass('inactive');
	$("#sidemenu").addClass('active');
});

// Home Button
$("#home-btn").click(function() {
	$("#home-screen").addClass('active');
	$(".menu").removeClass('active');
	$("#content-wrap").addClass('minimized');
});

// App
$(".app-icon").click(function() {
	$("#content-wrap").removeClass('minimized');
	setTimeout(function(){ $("#home-screen").removeClass('active'); }, 300);
});

// Overlay
$("#overlay").click(function () {
	$("#content-wrap").removeClass('inactive');
	$("#sidemenu").removeClass('active');
});

// Options
$("#options-btn").click(function() {
	$("#song-options").addClass('active');
});

// Bluetooth
$("#bluetooth-btn").click(function() {
	$("#bluetooth-devices").addClass('active');
});

// Bluetooth Menu
$("#bluetooth-devices ul li").click(function() {
	$(this).toggleClass('connected');
	$(this).siblings().removeClass('connected');
	
	if ($("#bluetooth-devices ul li").hasClass('connected')) {
		$("#sub-controls i.fa-bluetooth-b").addClass('active');
	} else {
		$("#sub-controls i.fa-bluetooth-b").removeClass('active');
	}
});

// Close Menu
$(".close-btn").click(function() {
	$(".menu").removeClass('active');
});

/*
 * Music Player
 * By Greg Hovanesyan
 * https://codepen.io/gregh/pen/NdVvbm
 */

window.addEventListener('mousedown', function(event) {
  
  if(!isDraggable(event.target)) return false;
  
  currentlyDragged = event.target;
  let handleMethod = currentlyDragged.dataset.method;
  
  this.addEventListener('mousemove', window[handleMethod], false);

  window.addEventListener('mouseup', () => {
    currentlyDragged = false;
    window.removeEventListener('mousemove', window[handleMethod], false);
    console.log('new position: ' + positionRelease );
    player_set_var('Position', Math.round( positionRelease ));
  }, false);
});

sliders.forEach(slider => {
  let pin = slider.querySelector('.pin');
  slider.addEventListener('click', rewind);
});

function isDraggable(el) {
  let canDrag = false;
  let classes = Array.from(el.classList);
  draggableClasses.forEach(draggable => {
    if(classes.indexOf(draggable) !== -1)
      canDrag = true;
  })
  return canDrag;
}

function getRangeBox(event) {
  let rangeBox = event.target;
  let el = currentlyDragged;
  if(event.type == 'click' && isDraggable(event.target)) {
    rangeBox = event.target.parentElement.parentElement;
  }
  if(event.type == 'mousemove') {
    rangeBox = el.parentElement.parentElement;
  }
  return rangeBox;
}

function getCoefficient(event) {
  let slider = getRangeBox(event);
	let screenOffset = document.querySelector("#screen").offsetLeft + 60;
  let K = 0;
	let offsetX = event.clientX - screenOffset;
	let width = slider.clientWidth;
	K = offsetX / width;
  return K;
}

function rewind(event) {
  let rangeBox = getRangeBox(event);
  let direction = rangeBox.dataset.direction;
	let screenOffset = document.querySelector("#screen").offsetLeft + 60;
	var min = screenOffset - rangeBox.offsetLeft;
	var max = min + rangeBox.offsetWidth;   
	if(event.clientX < min) { 
      positionRelease = 0;
      progress.style.width = 0 + '%';
    } else if (event.clientX > max) {
      positionRelease = trackLength;
      progress.style.width = 100 + '%';
    } else {
      var percent = ((trackLength * getCoefficient(event)) / trackLength) * 100;
      progress.style.width = percent + '%';
      positionRelease = trackLength * getCoefficient(event);
    }
}
